# Foodsafe App

Foodsafe is a iOS mobile application who allow to scan a product and get all the nutritionals informations of it.
It is feeds with the Open Food Fact open source database to guarantee the datas.

* Realized with Swift 3
* Fetch the data through a Java API (Spring Boot)

## Purpose

Project realized in the context of school.

The purpose was to learn to handle a big amount of datas and to retrieve it through a mobile application.

## Technologies

* Swift 3


