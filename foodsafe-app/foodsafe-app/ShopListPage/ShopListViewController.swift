//
//  ShopListViewController.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 16/10/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class ShopListViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var newListView: UIView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var productTextField: UITextField!
    @IBOutlet weak var saveListButton: UIButton!
    
    @IBOutlet weak var oldListsView: UIView!
    @IBOutlet weak var oldListsTableView: UITableView!
    
    @IBOutlet weak var tabBar: UITabBar!
    
    let myGreen = UIColor(red:0.39, green:0.58, blue:0.52, alpha:1.0)
    let myGray = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    
    var productList : [String] = [] {
        didSet {
            self.listTableView.reloadData()
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.delegate = self
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
    
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        view.addGestureRecognizer(swipeDown)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        applyDesign()
        
    }
    
    @objc func keyboardWillAppear() {
        self.listTableView.alwaysBounceVertical = false
    }
    
    @objc func keyboardWillDisappear() {
        self.listTableView.alwaysBounceVertical = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            dismissKeyboard()
        }
    }
    
    func applyDesign() {
        
        self.navigationController?.navigationBar.topItem?.title = "Liste de courses"
        self.navigationController?.navigationBar.barTintColor = myGray
        self.tabBar.barTintColor = myGray
        
        self.navigationItem.hidesBackButton = true
        
        let border = CALayer()
        let width = CGFloat(1.5)
        
        border.borderColor = myGreen.cgColor
        border.frame = CGRect(x: 0, y: productTextField.frame.size.height - width, width: productTextField.frame.size.width, height: productTextField.frame.size.height)
        border.borderWidth = width
    
        productTextField.layer.addSublayer(border)
        productTextField.layer.masksToBounds = true
    }

    @IBAction func switchView(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.newListView.isHidden = false
            self.oldListsView.isHidden = true
            break
        case 1:
            self.newListView.isHidden = true
            self.oldListsView.isHidden = false
            break
        default:
            self.newListView.isHidden = false
            self.oldListsView.isHidden = true
        }
    }
    
    @IBAction func touchAddProductToList(_ sender: Any) {
        if let p = self.productTextField.text {
            self.productList.append(p)
            self.productTextField.text = ""
        }
    }
    
    
    
}

extension ShopListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        print(indexPath)
        if (cell?.accessoryType == .checkmark) {
            cell?.accessoryType = .none
        } else {
            cell?.accessoryType = .checkmark
        }
        
    }
    
}

extension ShopListViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = self.productList[indexPath.row]
        cell.tintColor = myGreen
        return cell
    }
    
}

extension ShopListViewController : UITabBarDelegate {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            self.productList.remove(at: indexPath.row)
            for i in self.productList {
                print(i)
            }
        }
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            let settingsViewController = SettingsViewController()
            self.navigationController?.pushViewController(settingsViewController, animated: false)
            break
        case 1:
            let scanViewController = ScanViewController()
            self.navigationController?.pushViewController(scanViewController, animated: false)
            break
        case 2:
            let shopListViewController = ShopListViewController()
            self.navigationController?.pushViewController(shopListViewController, animated: false)
            break
        case 3:
            let historyViewController = HistoryViewController()
            self.navigationController?.pushViewController(historyViewController, animated: false)
            break
        default:
            break
        }
    }
    
}
