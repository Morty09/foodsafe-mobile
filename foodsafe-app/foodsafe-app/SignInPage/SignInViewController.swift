//
//  SignInViewController.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 02/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    
    var crud = UserCRUD()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        view.addGestureRecognizer(swipeDown)

        applyDesign()
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            dismissKeyboard()
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func applyDesign() {
        let borderName = CALayer()
        let borderFirstname = CALayer()
        let borderEmail = CALayer()
        let borderPwd = CALayer()
        let width = CGFloat(1.5)
        let myGreen = UIColor(red:0.39, green:0.58, blue:0.52, alpha:1.0)
        
        borderName.borderColor = myGreen.cgColor
        borderName.frame = CGRect(x: 0, y: nameTextField.frame.size.height - width, width: nameTextField.frame.size.width, height: nameTextField.frame.size.height)
        borderName.borderWidth = width
        
        borderFirstname.borderColor = myGreen.cgColor
        borderFirstname.frame = CGRect(x: 0, y: firstnameTextField.frame.size.height - width, width: firstnameTextField.frame.size.width, height: firstnameTextField.frame.size.height)
        borderFirstname.borderWidth = width
        
        borderEmail.borderColor = myGreen.cgColor
        borderEmail.frame = CGRect(x: 0, y: emailTextField.frame.size.height - width, width: emailTextField.frame.size.width, height: emailTextField.frame.size.height)
        borderEmail.borderWidth = width
        
        borderPwd.borderColor = myGreen.cgColor
        borderPwd.frame = CGRect(x: 0, y: pwdTextField.frame.size.height - width, width: pwdTextField.frame.size.width, height: pwdTextField.frame.size.height)
        borderPwd.borderWidth = width
        
        nameTextField.layer.addSublayer(borderName)
        nameTextField.layer.masksToBounds = true
        
        firstnameTextField.layer.addSublayer(borderFirstname)
        firstnameTextField.layer.masksToBounds = true
        
        emailTextField.layer.addSublayer(borderEmail)
        emailTextField.layer.masksToBounds = true
        
        pwdTextField.layer.addSublayer(borderPwd)
        pwdTextField.layer.masksToBounds = true
    }

    @IBAction func touchSignIn(_ sender: Any) {
        
        guard
            let nameTxt = self.nameTextField.text,
            let firstnameTxt = self.firstnameTextField.text,
            let emailTxt = self.emailTextField.text,
            let pwdTxt = self.pwdTextField.text else {
                return
        }
        
        crud.createUser(userLastname: nameTxt, userFirstname: firstnameTxt, userEmail: emailTxt, userPassword: pwdTxt) { (isCreate) in
            
            if (isCreate == "Inscription ok") {
                self.crud.getUserByEmail(userEmail: emailTxt, completion: { (usr) in
                    UserConnected.shared.setUserConnected(user: usr!)
                    print(UserConnected.shared.getUserConnected().id)
                    let scanViewController = ScanViewController()
                    self.navigationController?.pushViewController(scanViewController, animated: false)
                })
            }
            
        }

    }

    @IBAction func touchAlreadySubscribe(_ sender: Any) {
        let loginViewController = LoginViewController()
        navigationController?.pushViewController(loginViewController, animated: false)
    }

}
