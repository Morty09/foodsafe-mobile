//
//  HistoryViewController.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 16/10/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    
    var crud = ProductCRUD()
    var imgProduct = UIImageView()
    var codes : [String] = []
    var products : [Product] = [] {
        didSet {
            self.historyTableView.reloadData()
        }
    }
    
    let scoreAcolor = UIColor(red:0.22, green:0.49, blue:0.28, alpha:0.4)
    let scoreBcolor = UIColor(red:0.56, green:0.73, blue:0.28, alpha:0.4)
    let scoreCcolor = UIColor(red:0.96, green:0.80, blue:0.27, alpha:0.4)
    let scoreDcolor = UIColor(red:0.87, green:0.53, blue:0.19, alpha:0.4)
    let scoreEcolor = UIColor(red:0.83, green:0.30, blue:0.16, alpha:0.4)
    let myGreen = UIColor(red:0.39, green:0.58, blue:0.52, alpha:1.0)
    let myGray = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.delegate = self
        self.historyTableView.delegate = self
        self.historyTableView.dataSource = self
        self.historyTableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "Card")
        applyDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        codes = UserConnected.shared.getUserConnected().list_product_history!
        codes.reverse()
        getAllProducts()
    }
    
    func applyDesign() {
        self.navigationController?.navigationBar.topItem?.title = "Historique"
        self.navigationController?.navigationBar.barTintColor = myGray
        self.tabBar.barTintColor = myGray
        self.navigationItem.hidesBackButton = true
    }
    
    func getAllProducts() {
        for c in codes {
            crud.getProductByCode(code: c) { (product) in
                self.products.append(product!)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL, cell: TableViewCell) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                cell.imageCell.image = UIImage(data: data)
            }
        }
    }
    
    func switchNutriScore(value: String, cell: TableViewCell) {
        
        switch value {
        case "a":
            cell.nutritionCell.backgroundColor = scoreAcolor.withAlphaComponent(1.0)
            break
        case "b":
            cell.nutritionCell.backgroundColor = scoreBcolor.withAlphaComponent(1.0)
            break
        case "c":
            cell.nutritionCell.backgroundColor = scoreCcolor.withAlphaComponent(1.0)
            break
        case "d":
            cell.nutritionCell.backgroundColor = scoreDcolor.withAlphaComponent(1.0)
            break
        case "e":
            cell.nutritionCell.backgroundColor = scoreEcolor.withAlphaComponent(1.0)
            break
        default:
            cell.nutritionCell.backgroundColor = UIColor .gray
            break
        }
        
    }

}

extension HistoryViewController : UITableViewDelegate {
    
}

extension HistoryViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Card", for: indexPath) as! TableViewCell
        
        cell.nameCell.text = products[indexPath.row].productName
        if let url = URL(string: products[indexPath.row].imageUrl!) {
            cell.imageCell.contentMode = .scaleAspectFit
            self.downloadImage(from: url, cell: cell)
        }
        cell.brandCell.text = products[indexPath.row].brands
        self.switchNutriScore(value: products[indexPath.row].nutritionGrades!, cell: cell)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(products[indexPath.row].productName)
        let productViewController = ProductViewController()
        productViewController.currentProduct = products[indexPath.row]
        self.navigationController?.pushViewController(productViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
}

extension HistoryViewController : UITabBarDelegate {

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            let settingsViewController = SettingsViewController()
            self.navigationController?.pushViewController(settingsViewController, animated: false)
            break
        case 1:
            let scanViewController = ScanViewController()
            self.navigationController?.pushViewController(scanViewController, animated: false)
            break
        case 2:
            let shopListViewController = ShopListViewController()
            self.navigationController?.pushViewController(shopListViewController, animated: false)
            break
        case 3:
            let historyViewController = HistoryViewController()
            self.navigationController?.pushViewController(historyViewController, animated: false)
            break
        default:
            break
        }
    }
    
}
