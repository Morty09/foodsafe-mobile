//
//  TableViewCell.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 06/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var card: UIView!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var brandCell: UILabel!
    @IBOutlet weak var nutritionCell: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        applyDesign()
    }
    
    func applyDesign() {
        self.nutritionCell.layer.cornerRadius = self.nutritionCell.frame.size.width/2
        self.nutritionCell.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
