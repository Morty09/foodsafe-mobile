//
//  SettingsViewController.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 16/10/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var allergenTableView: UITableView!
    
    var crud = UserCRUD()
    var userConnected : User!
    var userAllergens : [String] = []
    
    let myGreen = UIColor(red:0.39, green:0.58, blue:0.52, alpha:1.0)
    let myGray = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    let lightGray = UIColor(red:0.96, green:0.96, blue:0.97, alpha:1.0)
    
    var saveBtn = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.delegate = self
        
        self.allergenTableView.delegate = self
        self.allergenTableView.dataSource = self
        self.allergenTableView.allowsSelection = false
        
        self.saveBtn = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(updateBtn))
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        view.addGestureRecognizer(swipeDown)
        
        applyDesign()
        
        feelView()
    }
    
    func feelView() {
        
        userConnected = UserConnected.shared.getUserConnected()
        
        self.textFieldName.text = userConnected.lastname
        self.firstnameTextField.text = userConnected.firstname
        self.emailTextField.text = userConnected.email
        self.pwdTextField.text = userConnected.password
    
        if (userConnected.list_allergen != nil) {
            self.userAllergens = userConnected.list_allergen!
        }
        
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            dismissKeyboard()
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func makeTableViewEditable(_ sender: Any) {
        self.allergenTableView.allowsSelection = true
        navigationItem.rightBarButtonItem = saveBtn
    }
    
    @objc func logout() {
        let loginViewController = LoginViewController()
        self.navigationController?.pushViewController(loginViewController, animated: false)
    }
    
    func applyDesign() {
        
        saveBtn.tintColor = UIColor .black
        //navigationItem.rightBarButtonItem = saveBtn
        
        let logoutBtn = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(logout))
        logoutBtn.tintColor = UIColor .black
        self.navigationItem.leftBarButtonItem = logoutBtn
        
        self.navigationItem.titleView?.tintColor = UIColor .white
        self.navigationItem.title = "Paramètres"
        self.tabBar.barTintColor = myGray
        
        self.navigationItem.hidesBackButton = true
        
        let borderName = CALayer()
        let borderFirstname = CALayer()
        let borderEmail = CALayer()
        let borderPwd = CALayer()
        let width = CGFloat(1.5)
        
        borderName.borderColor = lightGray.cgColor
        borderName.frame = CGRect(x: 0, y: textFieldName.frame.size.height - width, width: textFieldName.frame.size.width, height: textFieldName.frame.size.height)
        borderName.borderWidth = width
        
        borderFirstname.borderColor = lightGray.cgColor
        borderFirstname.frame = CGRect(x: 0, y: firstnameTextField.frame.size.height - width, width: firstnameTextField.frame.size.width, height: firstnameTextField.frame.size.height)
        borderFirstname.borderWidth = width
        
        borderEmail.borderColor = lightGray.cgColor
        borderEmail.frame = CGRect(x: 0, y: emailTextField.frame.size.height - width, width: emailTextField.frame.size.width, height: emailTextField.frame.size.height)
        borderEmail.borderWidth = width
        
        borderPwd.borderColor = lightGray.cgColor
        borderPwd.frame = CGRect(x: 0, y: pwdTextField.frame.size.height - width, width: pwdTextField.frame.size.width, height: pwdTextField.frame.size.height)
        borderPwd.borderWidth = width
        
        textFieldName.layer.addSublayer(borderName)
        textFieldName.layer.masksToBounds = true
        
        firstnameTextField.layer.addSublayer(borderFirstname)
        firstnameTextField.layer.masksToBounds = true
        
        emailTextField.layer.addSublayer(borderEmail)
        emailTextField.layer.masksToBounds = true
        
        pwdTextField.layer.addSublayer(borderPwd)
        pwdTextField.layer.masksToBounds = true
    }
    
    @IBAction func updateUser(_ sender: UIButton) {
        
        saveAppear()
        
        switch sender.tag {
        case 0:
            self.textFieldName.text = ""
            break
        case 1:
            self.firstnameTextField.text = ""
            break
        case 2: self.emailTextField.text = ""
            break
        case 3:
            self.pwdTextField.text = ""
            break
        default:
            break
        }
        
    }
    
    func saveAppear() {
        self.navigationItem.rightBarButtonItem = saveBtn
    }
    
    func saveDisappear() {
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @objc func updateBtn() {
        
        print("Update samere")
        
        guard
            let firstname = self.firstnameTextField.text,
            let lastname = self.textFieldName.text,
            let email = self.emailTextField.text,
            let password = self.pwdTextField.text else {
                return
        }
        
        //let userToUpdate = User(id: UserConnected.shared.getUserConnected().id, firstname: firstname, lastname: lastname, email: email, password: password)
        
        let userToUpdate = User(id: self.userConnected.id, firstname: firstname, lastname: lastname, email: email, password: password, list_allergen: self.userAllergens, list_shop_history: nil, list_shop: nil, list_product_history: nil)
        
        UserConnected.shared.setUserConnected(user: userToUpdate)
        
        crud.updateUser(newUser: userToUpdate) { (userUpdated) in
            print(userUpdated)
        }
        
        self.allergenTableView.allowsSelection = false
        self.textFieldName.isUserInteractionEnabled = false
        self.firstnameTextField.isUserInteractionEnabled = false
        self.emailTextField.isUserInteractionEnabled = false
        self.pwdTextField.isUserInteractionEnabled = false
        
    }
    
}

extension SettingsViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            let settingsViewController = SettingsViewController()
            self.navigationController?.pushViewController(settingsViewController, animated: false)
            break
        case 1:
            let scanViewController = ScanViewController()
            self.navigationController?.pushViewController(scanViewController, animated: false)
            break
        case 2:
            let shopListViewController = ShopListViewController()
            self.navigationController?.pushViewController(shopListViewController, animated: false)
            break
        case 3:
            let historyViewController = HistoryViewController()
            self.navigationController?.pushViewController(historyViewController, animated: false)
            break
        default:
            break
        }
    }
    
}

extension SettingsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        print(indexPath)
        if (cell?.accessoryType == .checkmark) {
            cell?.accessoryType = .none
            var count = 0
            for i in self.userAllergens {
                if (cell?.textLabel?.text == i) {
                    self.userAllergens.remove(at: count)
                }
                count+=1
            }
        } else {
            cell?.accessoryType = .checkmark
            self.userAllergens.append(Allergens.allergens[indexPath.row])
        }
        
        for i in userAllergens {
            print (i)
        }
    }
}

extension SettingsViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Allergens.allergens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        
        for i in self.userAllergens {
            if (i == Allergens.allergens[indexPath.row]) {
                cell.accessoryType = .checkmark
            }
        }
        
        
        cell.textLabel?.text = Allergens.allergens[indexPath.row]
        cell.tintColor = myGreen
        return cell
        
    }
    
    
    
    
}
