//
//  UserConnected.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 07/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation

class UserConnected {
    
    static let shared = UserConnected()
    
    var user: User?
    
    private init() {}
    
    func setUserConnected(user: User) {
        self.user = user
    }

    func getUserConnected() -> User {
        return self.user!
    }
    
}
