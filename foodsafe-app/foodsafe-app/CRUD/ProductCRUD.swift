//
//  Product.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 06/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation
import UIKit

class ProductCRUD : NSObject {
    
    var product : Product!
    
    func getProductByCode(code : String, completion: @escaping (_ result: Product?)->()) {
        
        guard let url = URL(string: URLs.apiProducts+"/code/"+code) else {
            return
        }
        
        print(url)
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: {(data, res, err) in

            guard
                let d = data,
                let obj = try? JSONSerialization.jsonObject(with: d, options: .allowFragments),
                let p = obj as? [String : Any] else {
                    return
            }
            
            DispatchQueue.main.async {
                self.product = Product(data: p)
                completion(self.product)
            }
        })
        task.resume()
    }
    
}
