//
//  UserCRUD.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 08/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation

class UserCRUD  {
    
    var user : User!
    
    func createUser(userLastname: String, userFirstname: String, userEmail: String, userPassword: String, completion: @escaping (_ result: String) -> ()) {
        
        guard let url = URL(string: URLs.apiUsers) else {
            return
        }
        print(url)
        
        let userJson : [String : Any] = ["lastname" : userLastname,
                                         "firstname" : userFirstname,
                                         "email" : userEmail,
                                         "password" : userPassword]
        
        print("JSON \(userJson)")
        let jsonData = try? JSONSerialization.data(withJSONObject: userJson, options: .prettyPrinted)
        print(jsonData!)
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if (responseJSON as? [String: Any]) != nil {
                print("Inscription ok")
            }
            DispatchQueue.main.async {
                completion("Inscription ok")
            }
        }
        task.resume()
    }
    
    func getUserByEmail(userEmail : String, completion: @escaping (_ result: User?) -> ()) {
        
        guard let url = URL(string: URLs.apiUsers+"/email="+userEmail) else {
            return
        }
        print(url)
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: {(data, res, err) in
            guard
                let d = data,
                let obj = try? JSONSerialization.jsonObject(with: d, options: .allowFragments),
                let user = obj as? [String : Any] else {
                    return
            }
            
            self.user = User(data: user)
            
            DispatchQueue.main.async {
                completion(self.user)
            }
        })
        task.resume()
    }
    
    func updateUser(newUser : User, completion: @escaping (_ result: String)->()) {
    
        guard let url = URL(string: URLs.apiUsers+"/"+newUser.id) else {
            return
        }
        print(url)
        
        let userJson : [String : Any] = ["lastname" : newUser.lastname,
                                         "firstname" : newUser.firstname,
                                         "email" : newUser.email,
                                         "password" : newUser.password,
                                         "list_allergen" : newUser.list_allergen as Any,
                                         "list_shop" : newUser.list_shop as Any,
                                         "list_product_history" : newUser.list_product_history as Any,
                                         "list_shop_history" : newUser.list_shop_history as Any]
        
        print("JSON \(userJson)")
        let jsonData = try? JSONSerialization.data(withJSONObject: userJson, options: .prettyPrinted)
        print(jsonData!)
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if (responseJSON as? [String: Any]) != nil {
                print("Inscription ok")
            }
        }
        task.resume()
        
    }
    
    
}
