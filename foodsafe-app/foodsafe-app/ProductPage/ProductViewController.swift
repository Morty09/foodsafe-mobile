//
//  ProductViewController.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 16/10/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {

    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    //@IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var ingredientstextProduct: UILabel!
    @IBOutlet weak var categoriesProduct: UILabel!
    @IBOutlet weak var originsProduct: UILabel!
    @IBOutlet weak var allergensProduct: UILabel!
    @IBOutlet weak var brandProduct: UILabel!
    @IBOutlet weak var warningMessage: UILabel!
    @IBOutlet weak var nutriScore: UISegmentedControl!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var nameProduct: UILabel!
    
    
    var code : String!
    var currentProduct : Product!
    var crud = ProductCRUD()
    
    let scoreAcolor = UIColor(red:0.22, green:0.49, blue:0.28, alpha:0.4)
    let scoreBcolor = UIColor(red:0.56, green:0.73, blue:0.28, alpha:0.4)
    let scoreCcolor = UIColor(red:0.96, green:0.80, blue:0.27, alpha:0.4)
    let scoreDcolor = UIColor(red:0.87, green:0.53, blue:0.19, alpha:0.4)
    let scoreEcolor = UIColor(red:0.83, green:0.30, blue:0.16, alpha:0.4)
    let myGreen = UIColor(red:0.39, green:0.58, blue:0.52, alpha:1.0)
    let myGray = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.delegate = self
        applyDesign()
        getProduct()
        checkAllergens()
    }
    
    func getProduct() {
        
        if self.currentProduct != nil {
            fillViewWithData()
        } else {
            guard let code = self.code else {
                return
            }
            
            crud.getProductByCode(code: code) { (result) -> () in
                self.currentProduct = result
                self.fillViewWithData()
            }
        }
    }
    
    func checkAllergens() {
        
        let allergensUser = UserConnected.shared.getUserConnected().list_allergen
        let allergensProduct = self.currentProduct.allergens
        
        let splitedArray = allergensProduct.split(separator: ",")
        
        var isAllergen = false
        for i in allergensUser! {
            for j in splitedArray {
                if (i.lowercased().folding(options: .diacriticInsensitive, locale: NSLocale.current) == String(j.lowercased().folding(options: .diacriticInsensitive, locale: NSLocale.current))) {
                    self.warningMessage.isHidden = false
                    self.warningMessage.text = "🚨 Attention ! Ce produit contient un ou plusieurs de vos allergens: \(i)"
                    isAllergen = true
                }
            }
        }
        
        if (!isAllergen) {
            self.warningMessage.isHidden = false
            self.warningMessage.textColor = UIColor .gray
            self.warningMessage.text = "❎ Aucun de vos allergènes n'est spécifiés... vérifiez tout de même les allergènes plus bas !"
        }
        
    }
    
    func fillViewWithData() {
        self.nameProduct.text = self.currentProduct.productName.isEmpty == false ? self.currentProduct.productName : "Non spécifié"
        self.ingredientstextProduct.text = self.currentProduct.ingredients_text.isEmpty == false ? self.currentProduct.ingredients_text : "Non spécifié"
        self.categoriesProduct.text = self.currentProduct.categories.isEmpty == false ? self.currentProduct.categories : "Non spécifié"
        self.originsProduct.text = self.currentProduct.origins.isEmpty == false ? self.currentProduct.origins : "Non spécifié"
        self.allergensProduct.text = self.currentProduct.allergens.isEmpty == false ? self.currentProduct.allergens : "Non spécifié"
        self.brandProduct.text = self.currentProduct.brands?.isEmpty == false ? self.currentProduct.brands : "Non spécifié"
        
        self.switchNutriScore(value: self.currentProduct.nutritionGrades ?? " ")
        
        if let url = URL(string: self.currentProduct.imageUrl!) {
            self.imgProduct.contentMode = .scaleAspectFit
            self.downloadImage(from: url)
        } else {
            self.imgProduct.contentMode = .center
            self.imgProduct.image = UIImage(named: "not-found")
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async {
                self.imgProduct.image = UIImage(data: data)
            }
        }
    }
    
    func switchNutriScore(value: String) {
        print(value)
        
        switch value {
            case "a":
                (nutriScore.subviews[0] as UIView).backgroundColor = scoreAcolor.withAlphaComponent(1.0)
                break
            case "b":
                (nutriScore.subviews[1] as UIView).backgroundColor = scoreBcolor.withAlphaComponent(1.0)
                break
            case "c":
                (nutriScore.subviews[2] as UIView).backgroundColor = scoreCcolor.withAlphaComponent(1.0)
                break
            case "d":
                (nutriScore.subviews[3] as UIView).backgroundColor = scoreDcolor.withAlphaComponent(1.0)
                break
            case "e":
                (nutriScore.subviews[4] as UIView).backgroundColor = scoreEcolor.withAlphaComponent(1.0)
                break
            default:
                break
        }
        
    }

    func applyDesign() {
        self.navigationController?.navigationBar.topItem?.title = "Produit"
        self.navigationController?.navigationBar.barTintColor = myGray
        self.tabBar.barTintColor = myGray
        
        self.navigationItem.hidesBackButton = true
        
        self.warningMessage.isHidden = true
        
        greenView.layer.cornerRadius = 3
        
        let font = UIFont.systemFont(ofSize: 22)
        nutriScore.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        
        (nutriScore.subviews[0] as UIView).backgroundColor = scoreAcolor
        (nutriScore.subviews[0] as UIView).layer.cornerRadius = 5.0
        (nutriScore.subviews[0] as UIView).clipsToBounds = true
        
        (nutriScore.subviews[1] as UIView).backgroundColor = scoreBcolor
        (nutriScore.subviews[1] as UIView).layer.cornerRadius = 5.0
        (nutriScore.subviews[1] as UIView).clipsToBounds = true
        
        (nutriScore.subviews[2] as UIView).backgroundColor = scoreCcolor
        (nutriScore.subviews[2] as UIView).layer.cornerRadius = 5.0
        (nutriScore.subviews[2] as UIView).clipsToBounds = true
        
        (nutriScore.subviews[3] as UIView).backgroundColor = scoreDcolor
        (nutriScore.subviews[3] as UIView).layer.cornerRadius = 5.0
        (nutriScore.subviews[3] as UIView).clipsToBounds = true
        
        (nutriScore.subviews[4] as UIView).backgroundColor = scoreEcolor
        (nutriScore.subviews[4] as UIView).layer.cornerRadius = 5.0
        (nutriScore.subviews[4] as UIView).clipsToBounds = true
    }
    
}

extension ProductViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            let settingsViewController = SettingsViewController()
            self.navigationController?.pushViewController(settingsViewController, animated: false)
            break
        case 1:
            let scanViewController = ScanViewController()
            self.navigationController?.pushViewController(scanViewController, animated: false)
            break
        case 2:
            let shopListViewController = ShopListViewController()
            self.navigationController?.pushViewController(shopListViewController, animated: false)
            break
        case 3:
            let historyViewController = HistoryViewController()
            self.navigationController?.pushViewController(historyViewController, animated: false)
            break
        default:
            break
        }
    }
    
}
