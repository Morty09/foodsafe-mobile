//
//  Ingredient.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 03/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation

class Ingredient {
    
    
    let id : String
    let ingredientName : String
    let productsIds : [String]
    
    init(id : String, ingredientName : String, productIds : [String]) {
        self.id = id
        self.ingredientName = ingredientName
        self.productsIds = productIds
    }
    
}
