//
//  Product.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 03/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation

class Product {
    
    let id : String
    let ingredients : [Dictionary<String, Any>]?
    let stores : String?
    let productName : String
    let nutrientLevels : Dictionary<String, Any>?
    let nutriments : Dictionary<String, Any>?
    let quantity : String?
    let ingredients_text : String
    let imageUrl : String?
    let tracesTags : [String]?
    let vitaminsTags : [String]?
    let categories : String
    let origins : String
    let code : String
    let allergens : String
    let nutritionGrades : String?
    let ingredientsTextWithAllergens : String?
    let labels : String?
    let brands : String?
    let allergensTags : [String]?
    let traces : String?

    init(id : String, ingredients : [Dictionary<String, Any>]? = nil, stores : String? = nil, productName : String, nutrientLevels : Dictionary<String, Any>? = nil, nutriments : Dictionary<String, Any>? = nil, quantity : String? = nil, ingredients_text : String, imageUrl : String? = nil, tracesTags : [String]? = nil, vitaminsTags : [String]? = nil, categories : String, origins : String, code : String, allergens : String, nutritionGrades : String? = nil, ingredientsTextWithAllergens : String? = nil, labels : String? = nil, brands : String? = nil, allergensTags : [String]? = nil, traces : String? = nil) {
        self.id = id
        self.ingredients = ingredients
        self.stores = stores
        self.productName = productName
        self.nutrientLevels = nutrientLevels
        self.nutriments = nutriments
        self.quantity = quantity
        self.ingredients_text = ingredients_text
        self.imageUrl = imageUrl
        self.tracesTags = tracesTags
        self.vitaminsTags = vitaminsTags
        self.categories = categories
        self.origins = origins
        self.code = code
        self.allergens = allergens
        self.nutritionGrades = nutritionGrades
        self.ingredientsTextWithAllergens = ingredientsTextWithAllergens
        self.labels = labels
        self.brands = brands
        self.allergensTags = allergensTags
        self.traces = traces
    }
    
    convenience init?(data : [String: Any]) {
        guard
            let id = data["_id"] as? String,
            let product_name = data["product_name"] as? String,
            let ingredients = data["ingredients_text"] as? String,
            let code = data["code"] as? String,
            let categories = data["categories"] as? String,
            let origins = data["origins"] as? String,
            let allergens = data["allergens"] as? String else {
                return nil
        }
        self.init(id : id,
                  ingredients : data["ingredients"] as? [Dictionary<String, Any>],
                  stores : data["stores"] as? String,
                  productName : product_name,
                  nutrientLevels : data["nutrient_levels"] as? Dictionary<String, Any>,
                  nutriments : data["nutriments"] as? Dictionary<String, Any>,
                  quantity : data["quantity"] as? String,
                  ingredients_text : ingredients,
                  imageUrl : data["image_url"] as? String,
                  tracesTags : data["traces_tags"] as? [String],
                  vitaminsTags : data["vitamins_tags"] as? [String],
                  categories : categories,
                  origins : origins,
                  code : code,
                  allergens : allergens,
                  nutritionGrades : data["nutrition_grades"] as? String,
                  ingredientsTextWithAllergens : data["ingredients_text_with_allergens"] as? String,
                  labels : data["labels"] as? String,
                  brands : data["brands"] as? String,
                  allergensTags : data["allergens_tags"] as? [String],
                  traces : data["traces"] as? String)
    }
    
}
