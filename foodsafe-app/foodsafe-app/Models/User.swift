//
//  User.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 02/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation

class User {
    
    let id : String
    let firstname : String
    let lastname : String
    let email : String
    let password : String
    let list_allergen : [String]?
    let list_shop_history : [[String]]?
    let list_shop : [String]?
    let list_product_history : [String]?
    
    init(id : String, firstname : String, lastname : String, email : String, password : String, list_allergen : [String]? = nil, list_shop_history : [[String]]? = nil, list_shop : [String]? = nil, list_product_history : [String]? = nil) {
        self.id = id
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password
        self.list_allergen = list_allergen
        self.list_shop_history = list_shop_history
        self.list_shop = list_shop
        self.list_product_history = list_product_history
    }
    
    convenience init?(data : [String: Any]) {
        guard
            let id = data["id"] as? String,
            let firstname = data["firstname"] as? String,
            let lastname = data["lastname"] as? String,
            let email = data["email"] as? String,
            let password = data["password"] as? String else {
                return nil
        }
        self.init(id: id,
                  firstname: firstname,
                  lastname: lastname,
                  email: email,
                  password: password,
                  list_allergen: (data["list_allergen"] as? [String]),
                  list_shop_history: (data["list_shop_history"] as? [[String]]),
                  list_shop: (data["list_shop"] as? [String]),
                  list_product_history: (data["list_product_history"] as? [String]))
    }
    
}
