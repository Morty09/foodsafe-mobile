//
//  LoginViewController.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 16/10/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    
    var userConnected : User!
    var crud = UserCRUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        view.addGestureRecognizer(swipeDown)
        
        UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .dark)
        
        applyDesign()
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            dismissKeyboard()
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func applyDesign() {
        let borderEmail = CALayer()
        let borderPwd = CALayer()
        let width = CGFloat(1.5)
        let myGreen = UIColor(red:0.39, green:0.58, blue:0.52, alpha:1.0)
        
        borderEmail.borderColor = myGreen.cgColor
        borderEmail.frame = CGRect(x: 0, y: emailTextField.frame.size.height - width, width: emailTextField.frame.size.width, height: emailTextField.frame.size.height)
        borderEmail.borderWidth = width
        
        borderPwd.borderColor = myGreen.cgColor
        borderPwd.frame = CGRect(x: 0, y: pwdTextField.frame.size.height - width, width: pwdTextField.frame.size.width, height: pwdTextField.frame.size.height)
        borderPwd.borderWidth = width
        
        emailTextField.layer.addSublayer(borderEmail)
        emailTextField.layer.masksToBounds = true
        
        pwdTextField.layer.addSublayer(borderPwd)
        pwdTextField.layer.masksToBounds = true
    }
    
    @IBAction func touchConnect(_ sender: Any) {
        guard
            let emailTxt = self.emailTextField.text,
            let pwdTxt = self.pwdTextField.text else {
            return
        }
        
        crud.getUserByEmail(userEmail: emailTxt) { (usr) in
            if (usr != nil &&
                usr?.email == emailTxt &&
                usr?.password == pwdTxt) {
                
                UserConnected.shared.setUserConnected(user: usr!)
                print(UserConnected.shared.getUserConnected().id)
                let scanViewController = ScanViewController()
                self.navigationController?.pushViewController(scanViewController, animated: false)
                
            } else {
                let alertController = UIAlertController(title: "Oopsss !", message: "Vos identifiants sont incorrects 😢", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok, je réessaie ! 💪🏼", style: .cancel))
                self.present(alertController, animated: true)
            }
        }
        
       
    }
    
    @IBAction func touchSignIn(_ sender: Any) {
        let signInViewController = SignInViewController()
        navigationController?.pushViewController(signInViewController, animated: false)
    }
    
    
}
