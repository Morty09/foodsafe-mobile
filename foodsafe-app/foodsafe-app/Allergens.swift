//
//  Allergens.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 13/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation


struct Allergens {
    static let allergens : [String] = [
        "Blé",
        "Seigle",
        "Orge",
        "Avoine",
        "Epeautre",
        "Kamut",
        "Céréales",
        "Crustacés",
        "Oeufs",
        "Poissons",
        "Arachides",
        "Soja",
        "Lactose",
        "Lait",
        "Lait de vache",
        "Lait de brebis",
        "Lait de chèvre",
        "Lait de buffelone",
        "Fruits à coque",
        "Noisette",
        "Amande",
        "Noix",
        "Noix de cajou",
        "Noix de pécan",
        "Noix de macadamia",
        "Noix du Brésil",
        "Noix de Queensland",
        "Pistaches",
        "Céléri",
        "Moutarde",
        "Graine de sésame",
        "Lupin",
        "Mollusque"
    ]
}
