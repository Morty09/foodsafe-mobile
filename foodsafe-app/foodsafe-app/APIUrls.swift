//
//  APIUrls.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 02/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation

struct URLs {
    static let apiProducts = "http://172.20.10.3:8983/products"
    static let apiUsers = "http://172.20.10.3:8983/users"
    static let apiIngredients = "http://172.20.10.3:8983/ingredients"
}
