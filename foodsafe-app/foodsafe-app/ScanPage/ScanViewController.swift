//
//  ScanViewController.swift
//  foodsafe-app
//
//  Created by Bérangère La Touche on 16/10/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import AVFoundation

class ScanViewController: UIViewController {
    
    let crudProduct = ProductCRUD()
    let crudUser = UserCRUD()
    let myGreen = UIColor(red:0.39, green:0.58, blue:0.52, alpha:1.0)
    let myGray = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var listCodeHistory: [String] = []
    var productDetected : Product!
    
    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.delegate = self
        applyDesign()
        initCameraForScan()
        UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .dark)
    }
    
    func applyDesign() {
        self.navigationController?.navigationBar.topItem?.title = "Scannez votre article"
        self.navigationController?.navigationBar.barTintColor = myGray
        self.tabBar.barTintColor = myGray
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
    }
    
    func initCameraForScan() {
        
        // Get the back-facing camera for capturing videos
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)

            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.frame.size = scanView.frame.size
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill

        scanView.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        qrCodeFrameView?.frame = CGRect(x: (videoPreviewLayer?.bounds.size.width)!/7 , y: (videoPreviewLayer?.bounds.size.height)!/3, width: 300, height: 200)
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = myGreen.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            scanView.addSubview(qrCodeFrameView)
            scanView.bringSubviewToFront(qrCodeFrameView)
        }
        
    }
    
    // MARK: - Helper methods
    func launchApp(decodedURL: String) {
        
        self.captureSession.stopRunning()
        
        crudProduct.getProductByCode(code: decodedURL) { (result) -> () in
            self.productDetected = result
            
            if (self.productDetected == nil) {
                let alertController = UIAlertController(title: "Oopsss !", message: "Il semblerait que ce produit n'existe pas 😢", preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: "Ok, je réessaie ! 💪🏼", style: .cancel)
                cancelButton.setValue(self.myGreen, forKey: "titleTextColor")
                alertController.addAction(cancelButton)
                
                self.present(alertController, animated: true)
                self.captureSession.startRunning()
            } else {
                if self.presentedViewController != nil {
                    return
                }
    
                self.updateHistoryOfCurrentUser()
                
                let productViewController = ProductViewController()
                productViewController.currentProduct = self.productDetected
                self.navigationController?.pushViewController(productViewController, animated: false)
            }
        }
    }
    
    func updateHistoryOfCurrentUser() {
        let currentUser = UserConnected.shared.getUserConnected()
        
        if let list = currentUser.list_product_history {
            self.listCodeHistory = list
            var isExist = false
            for i in self.listCodeHistory {
                if i == self.productDetected.code {
                    isExist = true
                }
            }
            if (!isExist) {
                self.listCodeHistory.append(self.productDetected.code)
            }
        } else {
            self.listCodeHistory.append(self.productDetected.code)
        }
        
        let newUser = User(id: currentUser.id, firstname: currentUser.firstname, lastname: currentUser.lastname, email: currentUser.email, password: currentUser.password, list_allergen: currentUser.list_allergen, list_shop_history: currentUser.list_shop_history, list_shop: currentUser.list_shop, list_product_history: self.listCodeHistory)
        UserConnected.shared.setUserConnected(user: newUser)
        
        self.crudUser.updateUser(newUser: newUser, completion: { (result) in
            print("Updated list ok")
        })
    }
    
}

extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                launchApp(decodedURL: metadataObj.stringValue!)
            }
        }
    }
    
}

extension ScanViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print(item.tag)
        switch item.tag {
        case 0:
            let settingsViewController = SettingsViewController()
            self.navigationController?.pushViewController(settingsViewController, animated: false)
            break
        case 1:
            let scanViewController = ScanViewController()
            self.navigationController?.pushViewController(scanViewController, animated: false)
            break
        case 2:
            let shopListViewController = ShopListViewController()
            self.navigationController?.pushViewController(shopListViewController, animated: false)
            break
        case 3:
            let historyViewController = HistoryViewController()
            self.navigationController?.pushViewController(historyViewController, animated: false)
            break
        default:
            break
        }
    }
    
}
